import React, { useState } from "react";
import { logo } from "../data";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import quotes from "../data";

const Wrapper2 = () => {
  const [count, setCount] = useState(0);
  const handleChange = () => {
    return count < quotes.length - 1 ? setCount(count + 1) : setCount(0);
  };

  return (
    
    <Card className="shadow rounded ">
      <Card.Body style={{ width: "50vw", height: "40vh" }} className="d-flex flex-column justify-content-between alighn-items-end">
        <Card.Text className=" fs-4">{quotes[count].quota}</Card.Text>
        <Card.Subtitle className="mb-2 text-muted">
          {quotes[count].autor}
        </Card.Subtitle>
        <div className="d-flex flex-row justify-content-between">
        <Card.Link  href="twitter.com/intent/tweet" target="_blank">
          {logo}
        </Card.Link>
        <Button className='btn btn-lg'
          variant="dark"
          onClick={() => {
            handleChange();
          }}
        >
          New quota
        </Button>
        </div>
        
      </Card.Body>
    </Card>
  );
};

export default Wrapper2;
